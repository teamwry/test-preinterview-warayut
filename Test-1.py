import random

T = []

#กำหนด Size ของ List
size = int(input("Size your list = "))
for i in range(size) :
    #ใส่หมายเลขลงใน List
    number = int(input("Number ="))
    T.append(number)
print(T)

class OddEvenList :
    def add(T):
        index = int(input("Enter index = "))
        try:
            print("Number is",T[index])
        except IndexError:
            print("More than List")

    def remove(T):
        print("List =",T)
        indexRemove = int(input("Enter index for remove = "))
        try:
            del T[indexRemove]
            print("Remove Success!!, New List  = ", T)
        except IndexError:
            print("Remove Number From Index Fail")
    
    def getOdd(T):
        oddList = []
        print("List =",T)
        for i in range(size):
            if i % 2 != 0 :
                oddList.append(T[i])
        print("Odd List = ",oddList)

    def getEven(T):
        evenList = []
        print("List =",T)
        for i in range(size):
            if i % 2 == 0 and i > 0 :
                evenList.append(T[i])
        print("Even List = ",evenList)

    def getRandom(T):
        print("List =",T)
        print("Random Number = ",random.choice(T))

#กำหนดชื่อ Function ในการเรียกใช้
function_dict = {'add':OddEvenList.add, 
                'remove':OddEvenList.remove, 
                'getOdd':OddEvenList.getOdd, 
                'getEven':OddEvenList.getEven, 
                'getRandom':OddEvenList.getRandom}

#command สำหรับการเรียก Class และ Method มาใช้งาน
#มี Function add, remove, getOdd, getEven, getRandom
if __name__ == "__main__":
    command = input("Select Function(add, remove, getOdd, getEven, getRandom) : ")
    function_dict[command](T)
